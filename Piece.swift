//
//  Piece.swift
//  Stratego
//
//  Created by kevin travers on 5/11/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Piece{
    //piece ablities 
    var pieceMovement:Int = 0
    var pieceName:String = "name"
    var pieceValue:Int = 0
    var pieceDescription:String = "placeholder"
    
    
    var isFlag:Bool = false
    var team:String = "Netrual"
    var isBomb:Bool = false
    
    var isActive:Bool = false
    var currentPostionX:Int = 0
    var currentPostionY:Int = 0
    
    init(name:String){
        team = name
        
    }
    func setTeam(name:String){
        team = name
    }
   
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setDescription(){
        //nothing this is overridden
        print("set description was called")
    }
    func getDescription()->String{
        return pieceDescription
    }
    func setMovement(){
        //nothing this is overridden
        print("set movement was called")
    }
    func getMovement()->Int{
        return pieceMovement
    }
    func setValue(){
        //nothing this is overridden
        print("set value was called")
    }
    func getValue()->Int{
        return pieceValue
    }
    func getname()->String{
        return pieceName
    }
    func setName(name:String){
        pieceName = name
    }
    func setBomb(){
        isBomb = true
    }
    func setFlag(){
        isFlag = true
    }
    
    func getBomb()->Bool{
        return isBomb
    }
    func getFlag()->Bool{
        return isFlag
    }
    func getTeam()->String{
        return team
    }
    
    func getCurrentX()->Int{
        return currentPostionX
    }
    func getCurrentY()->Int{
        return currentPostionY
    }
    func setCurrentX(x:Int){
        currentPostionX = x
    }
    func setCurrentY(y:Int){
        currentPostionY = y
    }
    func showValidMoves(board: Board)->[BoardSpot]{
        var validMoves: [BoardSpot] = []
        var blockUp = false
        var blockDown = false
        var blockRight = false
        var blockLeft = false
        if(getMovement() > 0){
            for location in 1...self.getMovement(){
                
                //check up/down left and right
                var tempX = currentPostionX
                var tempY = currentPostionY
                //check left
                tempX -= location
                if(!blockLeft){
                    if(validMove(tempX,y: tempY, board: board)){
                        if(checkForEnemy(tempX,y: tempY, board: board)){
                            blockLeft = true
                        }
                        validMoves.append(board.getBoardSpot(tempX, Y: tempY))
                        
                    }else{
                        blockLeft = true
                    }
                }
                
                tempX = currentPostionX
                tempX += location
                if(!blockRight){
                    if(validMove(tempX,y: tempY, board: board)){
                        if(checkForEnemy(tempX,y: tempY, board: board)){
                            blockRight = true
                        }
                        validMoves.append(board.getBoardSpot(tempX, Y: tempY))
                        
                    }else{
                        blockRight = true
                    }
                }
                
                tempX = currentPostionX
                tempY -= location
                if(!blockDown){
                    if(validMove(tempX,y: tempY, board: board)){
                        if(checkForEnemy(tempX,y: tempY, board: board)){
                            blockDown = true
                        }
                        validMoves.append(board.getBoardSpot(tempX, Y: tempY))
                        
                    }else{
                        blockDown = true
                    }
                }
                
                tempY = currentPostionY
                tempY += location
                if(!blockUp){
                    if(validMove(tempX,y: tempY, board: board)){
                        if(checkForEnemy(tempX,y: tempY, board: board)){
                            blockUp = true
                        }
                        validMoves.append(board.getBoardSpot(tempX, Y: tempY))
                        
                    }else{
                        blockUp = true
                    }
                }
                
                
            }

        }
        
        return validMoves
    }
    func validMove(x:Int, y:Int,board:Board)->Bool{
        var result = true
        //check if out of bounds
        
        if(x<0){
            result = false
        }else if(y<0){
            result = false
        }else if(x>board.baordSizeX){
            result = false
        }else if(y>board.baordSizeY){
            result = false
        }//check if hit same team unit is there 
        else if(board.getBoardSpot(x, Y: y).getPiece().getTeam() == self.getTeam()){
            result = false
        }//check for lake
        else if(board.getBoardSpot(x, Y: y).getPiece().getname() == "Lake"){
            result = false
        }
        return result
    }
    //true if enemy
    func checkForEnemy(x:Int, y:Int,board:Board)->Bool{
        var result = false
        if(!board.getBoardSpot(x, Y: y).isSpotEmpty()){
            result = true
        }
        return result
    }
    func getIsActive()->Bool{
        return isActive
    }
    
    //might not need this
    //moght do movement in matrix and not in peices
    func getPositionX()->Int{
        return currentPostionX
    }
    func getPositionY()->Int{
        return currentPostionY
    }
    
    func move(X:Int, Y:Int){
        currentPostionX = X
        currentPostionY = Y
    }
    
}
//
//  LoginPage.swift
//  Stratego
//
//  Created by kevin travers on 5/2/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit


var userImage = NSData()
var userName = ""
class LoginPage: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var profile: UIImageView!
    let loginButton: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["email"]
        return button
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(loginButton)
        loginButton.center = view.center
        loginButton.delegate = self
       
        if let token = FBSDKAccessToken.currentAccessToken(){
            fetchProfile()
        }
    }
    func fetchProfile(){
        print("fetch profile")
        let parameters = ["fields":"email, first_name, last_name, picture.type(large)"]
        let req = FBSDKGraphRequest(graphPath: "me", parameters: parameters , tokenString: FBSDKAccessToken.currentAccessToken().tokenString, version: nil, HTTPMethod: "GET")
            req.startWithCompletionHandler { (conection, result, error) -> Void in
            if ((error) != nil)
            {
                // Process error
                print(error)
            }
            else if (result.isCancelled != nil) {
                // Handle cancellations
                print("oh")
            }
            else {
               
                if let email = result.valueForKey("email") as? String{
                    print(email)
                }
                if let picture = result.valueForKey("picture") as? NSDictionary, data = picture.valueForKey("data") as? NSDictionary, url = data["url"] as? String  {
                    let urlPic = NSURL(string: url)
                    userImage = NSData(contentsOfURL: urlPic!)! //make sure your image in this url does exist, otherwise unwrap in a if let check
                    //userImage = data!
                    self.profile.image = UIImage(data: userImage)
                    
                }
                if let namef = result.valueForKey("first_name") as? String{
                    print("hello \(namef)")
                    
                    if let namel = result.valueForKey("last_name") as? String{
                        print("hello \(namel)")
                        userName = namef + " " + namel
                        self.performSegueWithIdentifier("GameViewController", sender: self)
                    }
                }
            }

        }
    }
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("User Logged In")
        fetchProfile()
            }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
}
//
//  Scout.swift
//  Stratego
//
//  Created by kevin travers on 5/13/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation

class Marshal:Piece{
    var myPieceName:String = "Marshal"
    var myPieceMovement:Int = 1
    var myPieceValue:Int = 10
    init(x:Int,y:Int,userName:String){
        super.init(name: userName)
        
        setMovement()
        setValue()
        setDescription()
        setCurrentX(x)
        setCurrentY(y)
        self.setName(myPieceName)
    }
    
    override func setMovement() {
        self.pieceMovement = myPieceMovement
    }
    override func setValue() {
        self.pieceValue = myPieceValue
    }
    override func setDescription() {
        self.pieceDescription = "Marshal can be captured by the Spy"
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
}
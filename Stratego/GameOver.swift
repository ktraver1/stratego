//
//  GameOver.swift
//  Stratego
//
//  Created by kevin travers on 5/15/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class GameOver: SKScene {
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let scene = SetUp(fileNamed:"SetUpScene") {
            // Configure the view.
            let skView = self.view
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView!.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView!.presentScene(scene)
        }
    }
}
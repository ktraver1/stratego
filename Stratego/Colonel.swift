//
//  Scout.swift
//  Stratego
//
//  Created by kevin travers on 5/13/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation

class Colonel:Piece{
    var myPieceName:String = "Colonel"
    var myPieceMovement:Int = 1
    var myPieceValue:Int = 8
    init(x:Int,y:Int,userName:String){
        super.init(name: userName)
        
        setMovement()
        setValue()
        setDescription()
        setCurrentX(x)
        setCurrentY(y)
        self.setName(myPieceName)
    }
    
    override func setMovement() {
        self.pieceMovement = myPieceMovement
    }
    override func setValue() {
        self.pieceValue = myPieceValue
    }
    override func setDescription() {
        self.pieceDescription = "The colonel has left his fired chicken franchise to fight for his counrty and glory. He also smells like chicken"
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
}
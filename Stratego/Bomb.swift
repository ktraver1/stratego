//
//  Scout.swift
//  Stratego
//
//  Created by kevin travers on 5/13/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation

class Bomb:Piece{
    var myPieceName:String = "Bomb"
    var myPieceMovement:Int = 0
    var myPieceValue:Int = 11
    init(x:Int,y:Int,userName:String){
        super.init(name: userName)
        
        setMovement()
        setValue()
        setBomb()
        setDescription()
        setCurrentX(x)
        setCurrentY(y)
        self.setName(myPieceName)
    }
    
    override func setMovement() {
        self.pieceMovement = myPieceMovement
    }
    override func setValue() {
        self.pieceValue = myPieceValue
    }
    override func setDescription() {
        self.pieceDescription = "Immovable; defeats any attacking piece except Miner"
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
}
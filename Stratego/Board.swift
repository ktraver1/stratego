//
//  Board.swift
//  Stratego
//
//  Created by kevin travers on 5/11/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class Board {
    var baordSizeX:Int = 9
    var baordSizeY:Int = 9
    var boardMatrix = Array<Array<BoardSpot>>()
    var playesTurn:String = "UserName"
    init(){
        for column in 0...baordSizeX {
            var columnArray = Array<BoardSpot>()
            for row in 0...baordSizeY {
                columnArray.append(BoardSpot(x: column,y: row))
            }
            boardMatrix.append(columnArray)
        }
        
    }
    
    func getBoardSpot(X:Int, Y:Int)->BoardSpot{
        return boardMatrix[X][Y]
    }
    func setBoardPiece(X:Int, Y:Int, piece:Piece){
        boardMatrix[X][Y].setPiece(piece)
    }
    
    
}
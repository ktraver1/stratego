//
//  SetUp.swift
//  Stratego
//
//  Created by kevin travers on 5/15/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
var playerPiecesArray = Array<Piece>()
var enemyPiecesArray = Array<Piece>()
class SetUp: SKScene {
    
    var choices = Array<SKSpriteNode>()
    var spawns = Array<SKSpriteNode>()
    var spawnIndex:Int = 0
    var numLeft: [Int] = [1,8,5,4,4,4,3,2,1,1,6,1]
    
    var myX = -1
    var myY = 0
    override func didMoveToView(view: SKView) {
        setUpChoices()
        setUpBoard()
        highLight()
    }
    
    func setUpChoices(){
        
        
        
        self.enumerateChildNodesWithName("//create[0-9]*") { (node, stop) -> Void in
            
            let spirte = node as! SKSpriteNode
            spirte.alpha = 1.0
            spirte.position = node.position
            spirte.zPosition = 1
            //spirte.name = node.name
            
            
            self.choices.append(spirte)
        }
        
        var spirte = childNodeWithName("createBomb") as! SKSpriteNode
        spirte.alpha = 1.0
        
        spirte.zPosition = 1
       
        
        self.choices.append(spirte)
        spirte = childNodeWithName("createFlag") as! SKSpriteNode
        spirte.alpha = 1.0
        
        spirte.zPosition = 1
        
        
        self.choices.append(spirte)
        
       
        
    }
    
    func setUpBoard(){
        
        self.enumerateChildNodesWithName("//spawn[0-9]*") { (node, stop) -> Void in
            
            let spirte = SKSpriteNode(imageNamed: "BluePlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
                       spirte.name = "sprite"
            
            self.addChild(spirte)
            
            self.spawns.append(spirte)
        }
        
    }
    func decrementplayerPiecesArray(index:Int){
        numLeft[index] -= 1
        updateLabel(index)
    }
    func getAmountLeft(index:Int)->Int{
        return numLeft[index]
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        //check if selected done and met all requirements
        for touch in touches{
            let location = touch.locationInNode(self)
            
            let node = nodeAtPoint(location)
            let tempNode = node.name!
            let value = tempNode.componentsSeparatedByString("create")

            if let x = Int(value[1]){
                let tempIndex = x-1
                if(getAmountLeft(tempIndex) > 0){
                    updateBoard(tempIndex)
                    decrementplayerPiecesArray(tempIndex)
                }
            }else if(value[1] == "Bomb"){
                if(getAmountLeft(10) > 0){
                    updateBoard(10)
                    decrementplayerPiecesArray(10)
                }
            }else if(value[1] == "Flag"){
                if(getAmountLeft(11) > 0){
                    updateBoard(11)
                    decrementplayerPiecesArray(11)
                }
            }else{
                print("select a peice")
            }
        }
        
        
    }
    
    func updateBoard(index:Int){
        spawns[spawnIndex].alpha = 1.0
        spawns[spawnIndex].zPosition = 2
        spawns[spawnIndex].texture = choices[index].texture
        //need to get piece x and y
        if(spawnIndex % 10 == 0 && spawnIndex != 0){
            myX = 0
            myY += 1
        }else{
            myX += 1
        }
        print(myX)
        print(myY)
        switch index {
        case 0:
            print("spy")
            let piece = Spy(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Spy(x: myX,y: myY,userName: "Blue")
            enemyPiecesArray.append(piece2)
            break
        case 1:
            print("scout")
            let piece = Scout(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Scout(x: myX,y: myY,userName: "Blue")
            enemyPiecesArray.append(piece2)
            break
        case 2:
            print("miner")
            let piece = Miner(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Miner(x: myX,y: myY,userName: "Blue")
            enemyPiecesArray.append(piece2)
            break
        case 3:
            print("Segeant")
            let piece = Segeant(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Segeant(x: myX,y: myY,userName: "Blue")
            enemyPiecesArray.append(piece2)
            break
        case 4:
            print("Lieutenant")
            let piece = Lieutenant(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Lieutenant(x: myX,y: myY,userName: "Blue")
            enemyPiecesArray.append(piece2)

            break
        case 5:
            print("Captain")
            let piece = Captian(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Captian(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
            break
        case 6:
            print("Major")
            let piece = Major(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Major(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
            break
        case 7:
            print("Colonel")
            let piece = Colonel(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Colonel(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
            break
        case 8:
            print("General")
            let piece = General(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = General(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
        case 9:
            print("Marshal")
            let piece = Marshal(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Marshal(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
            break
        case 10:
            print("Bomb")
            let piece = Bomb(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Bomb(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
            break
        case 11:
            print("Flag")
            let piece = Flag(x: myX,y: myY,userName: "Red")
            playerPiecesArray.append(piece)
            let piece2 = Flag(x: myX,y: myY,userName: "Red")
            enemyPiecesArray.append(piece2)
            break
        default:
            print("other")
            break
        }
        

        spawnIndex += 1
        highLight()
    }
    func highLight(){
        if(spawnIndex < 40){
            spawns[spawnIndex].alpha = 1.0
            spawns[spawnIndex].zPosition = 1
            
        }else{
            startGame()
        }
        
    }
    //loop throught and update labels
    func updateLabel(index:Int){
        let temp = index + 1
        var label = SKLabelNode()
        
        if(temp == 11){
            label = childNodeWithName("counterBomb") as! SKLabelNode
        }else if(temp == 12){
            label = childNodeWithName("counterFlag") as! SKLabelNode
        }else{
            label = childNodeWithName("counter\(temp)") as! SKLabelNode
        }
        
        label.text = "\(numLeft[index])"
    }
    override func update(currentTime: NSTimeInterval) {
        
    }
    func startGame(){
            if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            let skView = self.view
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView!.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView!.presentScene(scene)
        }
    }
}
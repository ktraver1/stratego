//
//  Scout.swift
//  Stratego
//
//  Created by kevin travers on 5/13/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation

class Scout:Piece{
    var myPieceName:String = "Scout"
    var myPieceMovement:Int = 10
    var myPieceValue:Int = 2
    init(x:Int,y:Int,userName:String){
        super.init(name: userName)
        
        setMovement()
        setValue()
        setDescription()
        setCurrentX(x)
        setCurrentY(y)
        self.setName(myPieceName)
    }
    
    override func setMovement() {
        self.pieceMovement = myPieceMovement
    }
    override func setValue() {
        self.pieceValue = myPieceValue
    }
    override func setDescription() {
        self.pieceDescription = "Scouts can move any distance in a straight line, without leaping over pieces / lakes"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
//
//  BoardSpot.swift
//  Stratego
//
//  Created by kevin travers on 5/11/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation
import SpriteKit

class BoardSpot{
    var xLocation:Int = 0
    var yLocation:Int = 0
    var myPiece:Piece
    var isEmpty:Bool = true
    init(x:Int,y:Int){
        xLocation = x
        yLocation = y
        myPiece = Piece(name: "Blank")
        myPiece.setName("Blank")
        isEmpty = true
    }
    
    
    func getPiece()->Piece{
        return myPiece
    }
    
    func isSpotEmpty()->Bool{
        return isEmpty
    }
    func setPiece(piece:Piece){
        myPiece = piece
        piece.setCurrentX(xLocation)
        piece.setCurrentY(yLocation)
        isEmpty = false
    }
    func removePiece(){
        isEmpty = true
        myPiece = Piece(name: "Blank")
        myPiece.setName("Blank")
    }
}
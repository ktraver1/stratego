//
//  Scout.swift
//  Stratego
//
//  Created by kevin travers on 5/13/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation

class Lake:Piece{
    var myPieceName:String = "Lake"
    var myPieceMovement:Int = 0
    
    init(x:Int,y:Int){
        super.init(name: myPieceName)
        
        setMovement()
        
        setDescription()
        setCurrentX(x)
        setCurrentY(y)
        self.setName(myPieceName)
    }
    
    override func setMovement() {
        self.pieceMovement = myPieceMovement
    }

    override func setDescription() {
        self.pieceDescription = "Lake, sadly know of your soliders can swim. "
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
}
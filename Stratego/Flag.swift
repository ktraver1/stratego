//
//  Scout.swift
//  Stratego
//
//  Created by kevin travers on 5/13/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import Foundation

class Flag:Piece{
    var myPieceName:String = "Flag"
    var myPieceMovement:Int = 0
    var myPieceValue:Int = 0
    init(x:Int,y:Int,userName:String){
        super.init(name: userName)
        
        setMovement()
        setValue()
        setFlag()
        setDescription()
        setCurrentX(x)
        setCurrentY(y)
        self.setName(myPieceName)
    }
    
    override func setMovement() {
        self.pieceMovement = myPieceMovement
    }
    override func setValue() {
        self.pieceValue = myPieceValue
    }
    override func setDescription() {
        self.pieceDescription = "Immovable; its capture ends the game. Who ever possesses the oppoents flag Will know what it means to have seen the top of the mountain, and people will worship you as though you were a god!"
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
 
    
}
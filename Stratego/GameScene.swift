//
//  GameScene.swift
//  Stratego
//
//  Created by kevin travers on 5/2/16.
//  Copyright (c) 2016 BunnyPhantom. All rights reserved.
//


//to do to clean up code
// remove souct... needing a start x and y


//need a 10
//need a S
//need a movement peice

//coustom layout
//pvp
import SpriteKit

class GameScene: SKScene {
    var myBoard:Board = Board()
    //board is fliped
    //[0][0] = a10
    var userBoard = Array<Array<SKSpriteNode>>()
    var gameOver:Bool = false
    var playersTurn:Bool = true
    var pieceSelected:Bool = false
    var teamName:String = "PlaceHolder"
    var highLightedMoves = Array<SKSpriteNode>()
    var currentPieceX = -1
    var currentPieceY = -1
    var myCamera = SKCameraNode()
    var battling:Bool = false
    let playerPiecesArraySize = 39
    
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        //create the board
        clearDescription()
        
        let node = childNodeWithName("userName") as! SKLabelNode
        node.text = userName
        
        //let nodeImage = childNodeWithName("userImage") as! SKSpriteNode
        //nodeImage.texture = SKTexture(data: userImage, size: CGSize(width: 50, height: 50))
        
        var red = Array<Piece>()
        /*
        var myScout = Scout(x: 0,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 1,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 2,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 3,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 4,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 5,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 6,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 7,y: 7,userName: "Red")
        red.append(myScout)
        myScout = Scout(x: 8,y: 7,userName: "Red")
        red.append(myScout)
        var myMiner = Miner(x: 9,y: 7,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 0,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 1,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 2,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 3,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 4,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 5,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 6,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 7,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 8,y: 8,userName: "Red")
        red.append(myMiner)
        myMiner = Miner(x: 9,y: 8,userName: "Red")
        red.append(myMiner)
        let flag = Flag(x: 6,y: 6,userName: "Red")
        red.append(flag)
        var blue = Array<Piece>()
        for col in 0...9 {
            for row in 0...1 {
                let myScout = Scout(x: col,y: row,userName: "Blue")
                blue.append(myScout)
            }
        }
        */
        /*for column in 0...myBoard.baordSizeX {
            for row in 0...myBoard.baordSizeY {
                
                
                let myScout = Scout()
                myBoard.setBoardPiece(column, Y: row, piece: myScout)
            }
            
        }*/
        /*for column in 0...myBoard.baordSizeX {
            for row in 0...myBoard.baordSizeY {
                print(myBoard.getBoardSpot(column, Y: row).getPiece().getname())
                print(myBoard.getBoardSpot(column, Y: row).getPiece().getMovement())
                print(myBoard.getBoardSpot(column, Y: row).getPiece().getValue())
            }
            
        }*/
        //getMoves(myBoard.getBoardSpot(2, Y: 2))
        
        
        
        setUpUserBoard()
        
        setUpBoard(playerPiecesArray)
        
        flipColor()
        
        setUpBoard(enemyPiecesArray)
        
        var lake = Lake(x: 2,y: 4)
        
        red.append(lake)
        lake = Lake(x: 3,y: 4)
        red.append(lake)
        lake = Lake(x: 6,y: 4)
        red.append(lake)
        lake = Lake(x: 7,y: 4)
        red.append(lake)
        
        lake = Lake(x: 2,y: 5)
        red.append(lake)
        lake = Lake(x: 3,y: 5)
        red.append(lake)
        lake = Lake(x: 6,y: 5)
        red.append(lake)
        lake = Lake(x: 7,y: 5)
        red.append(lake)
        setUpBoard(enemyPiecesArray)
        setUpBoard(red)
        //jsut grab username
        teamName = "Red"
        flipTeam()
        myCamera = childNodeWithName("myCamera") as! SKCameraNode
        
        
    }
    func flipColor(){
        for index in 0...playerPiecesArraySize{
            if(enemyPiecesArray[index].getCurrentY() == 0){
                enemyPiecesArray[index].setCurrentY(9)
            }else if(enemyPiecesArray[index].getCurrentY() == 1){
                enemyPiecesArray[index].setCurrentY(8)
            }
            else if(enemyPiecesArray[index].getCurrentY() == 2){
                enemyPiecesArray[index].setCurrentY(7)
            }else{
                 enemyPiecesArray[index].setCurrentY(6)
            }
            enemyPiecesArray[index].setTeam("Blue")
        }
    }
    func setUpBoard(teamPieces:Array<Piece>){
        
        for piece in teamPieces{
            
            let x = piece.getCurrentX()
            let y = piece.getCurrentY()
            print(x)
            print(y)
            
            myBoard.setBoardPiece(x, Y: y, piece: piece)
            //get correct image
            
            //userBoard[x][y].texture = SKTexture(imageNamed: "RedCastle")
            if(piece.getname() != "Lake"){
                setImage(piece, userPiece: userBoard[x][y])
                userBoard[x][y].name = "\(x):\(y)"
                userBoard[x][y].zPosition = 1
                userBoard[x][y].alpha = 1.0
            }else{
                userBoard[x][y].name = "Lake"
                userBoard[x][y].zPosition = 1
                userBoard[x][y].alpha = 0.0
            }
            
            
            userBoard[x][y].color = .blueColor()
        }
    }
    func setUpUserBoard(){
        var columnArray = Array<SKSpriteNode>()
        
        self.enumerateChildNodesWithName("//a[1-9]*") { (node, stop) -> Void in
            
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        
        userBoard.append(columnArray)
        
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//b[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//c[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//d[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//e[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//f[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//g[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//h[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//i[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
        }
        userBoard.append(columnArray)
        columnArray = Array<SKSpriteNode>()
        self.enumerateChildNodesWithName("//j[0-9]*") { (node, stop) -> Void in
            let spirte = SKSpriteNode(imageNamed: "RedPlain")
            spirte.alpha = 0.0
            spirte.position = node.position
            spirte.zPosition = -1
            spirte.name = "sprite"
            self.addChild(spirte)
            
            columnArray.append(spirte)
            
        }
        userBoard.append(columnArray)
        

    }
    
    
    func flipTeam(){
        if teamName == "Red"{
            teamName = "Blue"
        }else{
            teamName = "Red"
        }
        //have pass phone image
        //flip all peices
        
        for row in 0...myBoard.baordSizeX{
            for col in 0...myBoard.baordSizeY{
                let piece = myBoard.getBoardSpot(row, Y: col).getPiece()
                
                if(piece.getTeam() == teamName){
                    setImage(piece,userPiece: userBoard[row][col])
                }else{
                    reverse(piece,userPiece: userBoard[row][col])
                }
            }
        }
        
    }
    func setImage(piece:Piece, userPiece:SKSpriteNode){
        
        //special case for now need a 10
        let team = piece.getTeam()
        if(piece.isBomb){
            userPiece.texture = SKTexture(imageNamed: "\(team)Bomb")
        }else if(piece.isFlag){
            userPiece.texture = SKTexture(imageNamed: "\(team)Flag")
        }else if(piece.pieceName == "Lake"){
            userPiece.texture = SKTexture(imageNamed: "Lake")
        }else{
            userPiece.texture = SKTexture(imageNamed: "\(team)\(piece.getValue())")
        }
        
    }
    func reverse(piece:Piece, userPiece:SKSpriteNode){
        let team = piece.getTeam()
         userPiece.texture = SKTexture(imageNamed: "\(team)Castle")
    }
    func showDescription(piece:Piece){
        let label = childNodeWithName("Description") as! SKLabelNode
        label.text = piece.getDescription()
    }
    func clearDescription(){
        let label = childNodeWithName("Description") as! SKLabelNode
        label.text = ""
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches{
            clearDescription()
            if(battling){
                moveToBoardView()
                if(gameOver){
                    endGame()
                }
            }else{
                let location = touch.locationInNode(self)
                
                let node = nodeAtPoint(location)
                
                let locationXY = node.name!.componentsSeparatedByString(":")
                
                if let x = Int(locationXY[0]){
                    if let y = Int(locationXY[1]){
                        if(playersTurn){
                            
                            let piece = myBoard.getBoardSpot(x, Y: y).getPiece()
                            
                            if(piece.getTeam() == teamName){
                                
                                clearHighLight()
                                getMoves(myBoard.getBoardSpot(x, Y: y))
                                showDescription(piece)
                                pieceSelected = true
                                currentPieceX = piece.getCurrentX()
                                currentPieceY = piece.getCurrentY()
                            }else if(pieceSelected){
                                
                                pieceSelected = false
                                //check if clicked possible move
                                for move in highLightedMoves{
                                    if(node == move){
                                        
                                        movePiece(x, y: y, location: myBoard.getBoardSpot(currentPieceX, Y: currentPieceY))
                                        clearHighLight()
                                        if(gameOver){
                                            endGame()
                                        }
                                        flipTeam()
                                        
                                    }
                                }
                            }else{
                                print("not a valid move")
                                pieceSelected = false
                                clearHighLight()
                            }
                        }else{
                            print("wait your turn")
                            pieceSelected = false
                            clearHighLight()
                        }
                        
                    }else{
                        print("select a piece")
                        pieceSelected = false
                        clearHighLight()
                    }
                    
                }else{
                    print("select a piece")
                    pieceSelected = false
                    clearHighLight()
                }

            }
            
            
        }
       
    }
    func printMatrix(){
        var temp = ""
        for column in 0...myBoard.baordSizeX {
            temp = ""
            for row in 0...myBoard.baordSizeY {
                temp += "|" + myBoard.getBoardSpot(column, Y: row).getPiece().getname() + "|"
            }
            print(temp)
        }
    }
    func getMoves(location:BoardSpot){
        var moves: [BoardSpot] = []
        if(!location.isSpotEmpty()){
            moves = location.getPiece().showValidMoves(myBoard)
            highlightMoves(moves,location: location)
        }else{
            print("pick not epmty spot")
        }
        
    }
    func highlightMoves(moves:[BoardSpot],location:BoardSpot){
        
        for iterator in moves{
            let node = SKSpriteNode(imageNamed: "\(teamName)Plain")
            node.alpha = 0.25
            node.position = userBoard[iterator.xLocation][iterator.yLocation].position
            node.zPosition = 1
            node.name = "\(iterator.xLocation):\(iterator.yLocation)"
            addChild(node)
            highLightedMoves.append(node)
        }
        
    }
    func clearHighLight(){
        for move in highLightedMoves{
            move.removeFromParent()
        }
        highLightedMoves.removeAll()
    }
    //back and forth between the same two spaces for more than three consecutive turns is illegal try to put that in
    //x and y is new location
    func movePiece(x:Int, y:Int, location:BoardSpot){
        //move piece to new location
        //check for battle

        var battleResult = true
        if(checkForEnemy(x, y: y)){
            //have a battle
            battleResult = moveToBattleView(location, p2: myBoard.getBoardSpot(x, Y: y))
            //battleResult = battle(location, p2: myBoard.getBoardSpot(x, Y: y))
        }else{
            moveToNextTurn()
        }
        if(battleResult){
            //piece moved to new spot

            let peice = location.getPiece()
            moveSprite(x, newY: y, oldX: peice.getCurrentX(), oldY: peice.getCurrentY(),imageName: peice.getname())
            myBoard.setBoardPiece(x, Y: y, piece: peice)
           
            
            //piece remove from old spot
            location.removePiece()

        }
        
    }
    func moveToNextTurn(){
        battling = true
        myCamera.position.x = -370
    }
    func moveToBattleView(p1:BoardSpot, p2:BoardSpot)->Bool{
        battling = true
        myCamera.position.x = 742.5
        let p1Piece = p1.getPiece()
        let p2Piece = p2.getPiece()
        let redLabel = childNodeWithName("RedLabel") as! SKLabelNode
        let blueLabel = childNodeWithName("BlueLabel") as! SKLabelNode
        let resultLabel = childNodeWithName("resultLabel") as! SKLabelNode
        let red = childNodeWithName("p2") as! SKSpriteNode
        let blue = childNodeWithName("p2") as! SKSpriteNode
        if(p1Piece.getTeam() == "Red"){
            if(p2.getPiece().isBomb){
                red.texture = SKTexture(imageNamed: "\(p2.getPiece().getTeam())Bomb")
            }else if(p2.getPiece().isFlag){
                red.texture = SKTexture(imageNamed: "\(p2.getPiece().getTeam())Flag")
            }
            redLabel.text = "\(p1Piece.getValue())"
            blueLabel.text = "\(p2Piece.getValue())"
        }else{
            if(p2.getPiece().isBomb){
                blue.texture = SKTexture(imageNamed: "\(p2.getPiece().getTeam())Bomb")
            }else if(p2.getPiece().isFlag){
                blue.texture = SKTexture(imageNamed: "\(p2.getPiece().getTeam())Flag")
            }
            redLabel.text = "\(p2Piece.getValue())"
            blueLabel.text = "\(p1Piece.getValue())"
        }
        let result = battle(p1, p2: p2)
        if(result){
            //p1 won
            resultLabel.text =  "\(p1.getPiece().getTeam()) is the Winner!"
        }else{
            if(p1.getPiece().getTeam() == "Blank"){
                 resultLabel.text =  "Tie both get Removed!"
            }else{
                resultLabel.text =  "\(p2.getPiece().getTeam()) is the Winner!"
            }
            
        }
        return result
        
    }
    func moveToBoardView(){
        myCamera.position.x = 187.5
        battling = false
    }
    //xy new location
    func moveSprite(newX:Int, newY:Int, oldX:Int, oldY:Int, imageName:String){

        //userBoard[newX][newY].texture =  SKTexture(imageNamed: "RedCastle")
        setImage(myBoard.getBoardSpot(newX, Y: newY).getPiece(), userPiece: userBoard[newX][newY])
        userBoard[newX][newY].alpha = 1.0
        userBoard[newX][newY].name = "\(newX):\(newY)"
        userBoard[newX][newY].zPosition = 1
        
        userBoard[oldX][oldY].alpha = 0.0
        userBoard[oldX][oldY].name = "sprite"
        userBoard[oldX][oldY].zPosition = -1
        
        
        
    
    }
    func checkForEnemy(x:Int, y:Int)->Bool{
        var result = false
        if(!myBoard.getBoardSpot(x, Y: y).isSpotEmpty()){
            result = true
        }
        return result
    }
    //p1 attacker
    //p2 defender
    func battle(p1:BoardSpot, p2:BoardSpot)->Bool{
        
        var result = true
        let p1Piece = p1.getPiece()
        let p2Piece = p2.getPiece()
        //check for special cases
        //flag game over
        if(p2.getPiece().getFlag()){
            gameOver = true
        }
        //miner vs bomb
        else if(p1Piece.getname()=="Miner" && p2Piece.getBomb()){
            removeSprite(userBoard[p2Piece.currentPostionX][p2Piece.currentPostionY])
            p2.removePiece()
            
        }
        else if(p1Piece.getname()=="Spy" && p2Piece.getname()=="Marshal"){
            removeSprite(userBoard[p2Piece.currentPostionX][p2Piece.currentPostionY])
            p2.removePiece()
            
        }
        //DEATHBATTLE
        //p1 is stronger
        else if(p1Piece.getValue() > p2Piece.getValue()){
            removeSprite(userBoard[p2Piece.currentPostionX][p2Piece.currentPostionY])
            p2.removePiece()
            
        }//p2 is stronger
        else if(p1Piece.getValue() < p2Piece.getValue()){
            result = false
            removeSprite(userBoard[p1Piece.currentPostionX][p1Piece.currentPostionY])
            p1.removePiece()
            
        }//tie
        else{
            result = false
            removeSprite(userBoard[p1Piece.currentPostionX][p1Piece.currentPostionY])
            removeSprite(userBoard[p2Piece.currentPostionX][p2Piece.currentPostionY])
            p1.removePiece()
            p2.removePiece()
            
        }
        return result
    }
    
    func removeSprite(sprite:SKSpriteNode){
        sprite.alpha = 0.00
        sprite.zPosition = -1
        sprite.name = "sprite"
    }
    
    func endGame(){
        if let scene = GameOver(fileNamed:"GameOverScene") {
            // Configure the view.
            
            if let skView = view{
                skView.showsFPS = false
                skView.showsNodeCount = false
                
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .AspectFill
                
                skView.presentScene(scene)
            }
            
            
        }

    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
    }
}
